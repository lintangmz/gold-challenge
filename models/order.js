'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Order extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  Order.init({
    userId: DataTypes.INTEGER,
    itemId: DataTypes.INTEGER,
    tglTransaksi: DataTypes.DATE,
    status: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Order',
  });

  Order.associate = function(models) {
    Order.hasMany(models.User, {
      foreignKey: 'orderId',
      as: 'users'
    })

    Order.hasMany(models.Item, {
      foreignKey: 'orderId',
      as: 'items'
    })
  }
  return Order;
};