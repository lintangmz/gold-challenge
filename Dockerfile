FROM node:16

WORKDIR D:\BINAR\gold_challenge

COPY package.json ./
COPY package-lock.json ./

RUN npm install
RUN npm install --save pm2

COPY . .

EXPOSE 3000

CMD ["npm", "run", "pm2"]